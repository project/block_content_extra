<?php

namespace Drupal\block_content_extra\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a node operations bulk form element.
 *
 * @ViewsField("block_content_bulk_form")
 */
class BlockContentBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No content selected.');
  }
}
