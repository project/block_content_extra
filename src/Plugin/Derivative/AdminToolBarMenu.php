<?php

namespace Drupal\block_content_extra\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminToolBarMenu extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(ModuleHandlerInterface $moduleHandler, EntityTypeManagerInterface $entityTypeManager) {
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    if ($this->moduleHandler->moduleExists('admin_toolbar')) {
      $links['block_content_extra.view.block_content'] = [
        'title' => t('Custom block library'),
        'provider' => 'block_content_extra',
        'route_name' => 'entity.block_content_extra_block_content.collection',
        'parent' => 'system.admin_content',
      ] + $base_plugin_definition;

      $storage = $this->entityTypeManager->getStorage('block_content_type');
      foreach ($storage->loadMultiple() as $block_type) {
        $links['block_content_extra.view.add_block_content_' . $block_type->id()] = [
          'title' => t('Add :type', [':type' => $block_type->label()]),
          'provider' => 'block_content_extra',
          'route_name' => 'block_content.add_form',
          'parent' => 'block_content_extra.admin_toolbar:block_content_extra.view.block_content',
          'route_parameters' => ['block_content_type' => $block_type->id()],
        ] + $base_plugin_definition;
      }
    }

    return $links;
  }

}
