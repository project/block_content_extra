<?php

namespace Drupal\block_content_extra;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class BlockContentTypeAccessControlHander.
 *
 * @package Drupal\block_content_extra
 */
class BlockContentTypeAccessControlHander extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'access block content overview');
        break;

      default:
        return parent::checkAccess($entity, $operation, $account);
    }
  }

}
